# AutoML with TPOT

This project provides a (relatively) in depth analysis of **[TPOT](https://epistasislab.github.io/tpot/)**, 
it's functionalities, and capabilities. This can be found in Presentation/

We also empirically compare TPOT (Tree based Pipeline Optimization Tool) with a Random Search hypertuned Random 
Forest and two other popular AutoML packages - **H2O** and **Auto-Sklearn**

We run equally timed experiments across 3 datasets from [a TPOT Evaluation paper (2019)](https://link.springer.com/chapter/10.1007/978-3-030-05318-5_8), 
3 datasets from [OpenML AutoML Benchmark Datasets], and  1 additional dataset from general OpenML. 
All algorithms are run 3 times to allow us to report a stable mean and 
the standard deviation.

We hope the code or our analysis will help you select an AutoML 
best suited to your needs!

## Instructions

Upload the ipynb in Notebooks/ and requirements.txt at root to Colab,
or run locally on jupyter notebook. In the latter case, using a virtual environment is 
recommended

Alternatively, our Colab Notebook can be [found here](https://colab.research.google.com/drive/1bmvqM_n4y9BCo-kzszzieWTSYApbuNRR#scrollTo=_Ce7vyFyrXqW). 
The requirements.txt must still be uploaded.

Either way, our  computed outputs and graphs should be saved within the ipynb on gitlab.

<!--## Scope of the Project-->

<!--- Present the TPOT toolkit. -->
<!--- Summarise the implemented optimization process-->
<!--- Explain the evolutionary algorithm-->
<!--- Overview over the implemented pipeline operators-->
<!--- Recursive feature elemination-->
<!--- Showcase some pipelines-->
<!--- Discuss the benchmark in the linked paper-->
<!--```-->
<!--https://epistasislab.github.io/tpot/-->
<!--https://dl.acm.org/doi/10.1145/2908812.2908918-->
<!--```-->
<!--Notebook: Benchmark vs. a baseline approach (random forest with random search) and another optimizer on some data from OpenML. -->


# References

* Olson, Randal S., et al. "Evaluation of a tree-based pipeline optimization tool for automating data science." Proceedings of the Genetic and Evolutionary Computation Conference 2016. 2016.
* Olson, Randal S., and Jason H. Moore. "TPOT: A tree-based pipeline optimization tool for automating machine learning." Automated Machine Learning. Springer, Cham, 2019. 151-160.
* Adithya Balaji and Alexander Allen, “Benchmarking Automatic Machine Learning Frameworks”. 2018. 
